# LibreGates

A Libre VHDL framework for the static and dynamic analysis of mapped, gate-level circuits for FPGA and ASIC. Design For Test or nothing !

See http://libregates.org

tl;dr:
This project uses pure GHDL-driven code to allow users to check their circuit, verify BIST coverage and eventually generate test vectors with only a set of really Libre files in pure VHDL (thus avoiding expensive and restrictive license fees from niche proprietary vendors). 

________________________________________________

LibreGates contains a set of tools that process VHDL files mapped to FPGA/ASIC/arbitrary logic gates, so it is useful as a step between the synthesis of a circuit and the place&route operation. This is an exploratory toolset, constrained to a specific step of the logic design workflow, as it does not modify or transform the source netlist but gives insight into its often invisible structure, and guides the designer while writing it, in an interactive, iterative process, while focusing on specific digital units. As such, it is complementary but not a replacement of abc or yosys, which have way broader reach and features.

You can:

- Simulate the circuit (for example, if your synthesiser has mapped the gates to a given PDK but you don't have the corresponding gates definitions in VHDL)
- Perform static analysis of the netlist (spot unconnected inputs or outputs, and other common mistakes, beyond what VHDL already catches by default)
- Extract dynamic activity statistics (how often does a wire flip state, if at all, during typical operation ?)
- Verify that any internal state can be reached (thus helping with logic simplifications and identify blind spots)
- Alter any boolean function (after synthesis), inject arbitrary errors, optimise and prove your BIST strategy
- Extract logic traversal depth and estimate speed/latency (roughly)
- Inspect logic cones, see what inputs and outputs affect what
- Help with replacing DFFs with transparent latches
- Ensure that the circuit is correctly initialised with the minimal amount of /RESET signals (saving routing and area)
- Detect and break unexpected logic loops or chains

Some day, it could be extended to:

- Pipeline a netlist and choose the appropriate strategy (will require detailed timing information, yosys probably does this already ?)
- Customise the set of available gates to emulate or substitute arbitrary PDK/ALIs (importing .liberty files).
- Transcode/Transpile a netlist from one family/technology/ALI to another
- Which also means the ability to create custom ALIs from analysis of a design unit...
- Import/export to Liberty, EDIF, Verilog, FPGA bitstream or others ?

Note: Since the tool typically processes netlists before place&route, no wiring parasitics data are available yet so no precise timing extraction is possible and it doesn't even try. It can however help, in particular with extraction of the criticality of each path then the mapping of gates to the proper fanout.

The project started as a "VHDL library for gate-level verification" but the scope keeps extending and greatly surpasses the mere ProASIC3 domain. For example I also study the addition of the minimalist OSU FreePDK45. More unrelated libraries would be added in the near future, depending on applications : Skywater PDK, Chip4Makers' FlexCell and Alliance could follow. Contact me if you need something !
